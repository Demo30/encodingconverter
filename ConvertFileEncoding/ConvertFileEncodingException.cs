﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertFileEncoding
{
    class ConvertFileEncodingException : Exception
    {
        public ConvertFileEncodingException() { }
        public ConvertFileEncodingException(string message) : base (message) { }
    }
}
