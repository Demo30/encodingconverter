﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertFileEncoding
{
    internal class ConvertorOptions
    {
        public string FolderPath { get; internal set; }
        public Encoding SourceEncoding { get; internal set; }
        public Encoding OutputEncoding { get; internal set; }
        public string SaveToSeparateDestination { get; set; }
        public string[] consideredFileExtensions { get; set; } = new string[0];

        public bool IsSetupValid()
        {
            if ((String.IsNullOrEmpty(this.FolderPath)) || this.SourceEncoding == null || this.OutputEncoding == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
