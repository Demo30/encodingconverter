﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConvertFileEncoding
{
    internal class Convertor
    {
        public string SeparateDestinationForOutput { get; set; }
        private ConvertorOptions _convertorOptions = null;

        public bool Convert(ConvertorOptions options, out Exception ErrorMessage)
        {
            if (options == null || !options.IsSetupValid())
            {
                ErrorMessage = new ArgumentException("Options argument is either null or is not in a valid state.");
                return false;
            }
            else
            {
                this._convertorOptions = options;
            }

            bool result = false;
            try
            {
                this.ConversionCore(this._convertorOptions.FolderPath);
                ErrorMessage = null;
                result = true;
            }
            catch(Exception ex)
            {
                ErrorMessage = ex;
                result = false;
            }

            return result;   
        }

        private void ConversionCore(string pathToFolder)
        {
            if (!Directory.Exists(this._convertorOptions.FolderPath))
            {
                throw new ConvertFileEncodingException("Specified folder path does not exist: " + this._convertorOptions.FolderPath);
            }

            string[] containedDirs = Directory.GetDirectories(pathToFolder);
            foreach (string curDir in containedDirs)
            {
                this.ConversionCore(curDir);
            }

            // could probably be done simpler with the GetFiles overloads...
            string[] containedFilePaths = Directory.GetFiles(pathToFolder);
            for (int i = 0; i < containedFilePaths.Length; i++)
            {
                string filePath = containedFilePaths[i];
                FileInfo fi = new FileInfo(filePath);

                if (this._convertorOptions.consideredFileExtensions != null && this._convertorOptions.consideredFileExtensions.Length > 0)
                {
                    string ext = fi.Extension.TrimStart('.');
                    if (!this._convertorOptions.consideredFileExtensions.Contains(ext))
                    {
                        continue;
                    }
                }

                Encoding source = this._convertorOptions.SourceEncoding;
                Encoding output = this._convertorOptions.OutputEncoding;

                byte[] sourceBytes = File.ReadAllBytes(filePath);
                byte[] convertedBytes = Encoding.Convert(source, output, sourceBytes);
                File.WriteAllBytes(filePath, convertedBytes);

                //String finalString = output.GetString(convertedBytes);
            }


        }
    }
}
