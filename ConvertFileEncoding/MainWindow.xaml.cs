﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.ComponentModel;

namespace ConvertFileEncoding
{
    public partial class MainWindow : Window
    {
        private string _selectedFolderPath = null;
        private Encoding _selectedEncodingSource = null;
        private Encoding _selectedEncodingOutput = null;
        private string[] _consideredFileExtensions = new string[0];
        private BackgroundWorker backgroundWorker = null;
        

        public MainWindow()
        {
            InitializeComponent();
            this.PrepareControls();
        }

        private void PrepareControls()
        {
            this.PrepareEncodingComboboxes();
        }

        private void PrepareEncodingComboboxes()
        {
            EncodingInfo[] options = Encoding.GetEncodings();
            this.cmbx_Encoding_Source.ItemsSource = options;
            this.cmbx_Encoding_Source.SelectedIndex = 0;
            this.cmbx_Encoding_Source.DisplayMemberPath = "DisplayName";
            this.cmbx_Encoding_Output.ItemsSource = options;
            this.cmbx_Encoding_Output.SelectedIndex = 0;
            this.cmbx_Encoding_Output.DisplayMemberPath = "DisplayName";
        }

        private void btn_ChooseFolder_Click(object sender, RoutedEventArgs args)
        {
            string selectedFolderPath = ChooseFolderExtended();

            if (selectedFolderPath == null) { return; }
            else
            {
                this.UpdateSelectedFolderPath(selectedFolderPath);
            }
        }

        private void btn_run_Click(object sender, RoutedEventArgs args)
        {
            if (String.IsNullOrEmpty(this._selectedFolderPath))
            {
                System.Windows.Forms.MessageBox.Show("Není vybraná žádná složka");
                return;
            }

            this.loadFileExtensions();

            string message = String.Format("Skutečně chcete projít celou stromovou strukturu složky {0} a překonvertovat soubory do nového kódování dle specifikace?", this._selectedFolderPath);
            DialogResult prompt = System.Windows.Forms.MessageBox.Show(message, "Varování", MessageBoxButtons.YesNoCancel);

            if (prompt == System.Windows.Forms.DialogResult.Yes)
            {
                if (this.backgroundWorker != null && this.backgroundWorker.IsBusy == true)
                {
                    System.Windows.Forms.MessageBox.Show("Operace je zrovna v procesu. Nelze pokračovat. Nejprve je třeba první ukončit.", "Nelze pokračovat");
                }
                else if (this.backgroundWorker == null)
                {
                    this.SetBackgroundWorker();
                    this.backgroundWorker.RunWorkerAsync(this.GetConvertorOptions());
                }
                else
                {
                    this.backgroundWorker.RunWorkerAsync(this.GetConvertorOptions());
                }
            }
        }

        private void btn_Hint_Click(object sender, RoutedEventArgs args)
        {
            string message =
                "Maska není nijak propracovaná, v tuto chvíli umožnuje pouze specifikaci přípon, co chceme zvažovat." + Environment.NewLine +
                "Prázdné pole znamená zvažovat všechny soubory. Jinak vypsat přípony bez tečky, oddělené čárkou bez mezer.";
            System.Windows.Forms.MessageBox.Show(message);
        }

        private void cmbx_Encoding_SelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            if (!(sender is System.Windows.Controls.ComboBox))
            {
                throw new ConvertFileEncodingException(String.Format("Control of type {0} expected.", nameof(System.Windows.Controls.ComboBox)));
            }
            System.Windows.Controls.ComboBox combobox = (System.Windows.Controls.ComboBox)sender;
            string controlName = combobox.Name;

            if (!(combobox.SelectedValue is EncodingInfo))
            {
                throw new ConvertFileEncodingException("Internal error. Combobox includes unexpected values.");
            }

            EncodingInfo value = (EncodingInfo)combobox.SelectedValue;

            switch (controlName)
            {
                case "cmbx_Encoding_Source":
                    this._selectedEncodingSource = value.GetEncoding();
                    break;
                case "cmbx_Encoding_Output":
                    this._selectedEncodingOutput = value.GetEncoding();
                    break;
                default:
                    throw new ConvertFileEncodingException("No action implemented for control of such name: " + controlName);
            }
        }

        private void UpdateSelectedFolderPath(string newFolderPath)
        {
            this._selectedFolderPath = newFolderPath;
            this.lbl_SourcePath.Content = this._selectedFolderPath;
        }

        private void loadFileExtensions()
        {
            string raw = this.txtbx_filePathMask.Text;
            string[] chunks = raw.Split(',');

            for (int i = 0; i < chunks.Length; i++)
            {
                string chunk = chunks[i].TrimStart(' ');
                chunk = chunk.TrimEnd(' ');
                chunks[i] = chunk;
            }

            this._consideredFileExtensions = chunks;
        }

        private string ChooseFolderNative()
        {
            using (FolderBrowserDialog dia = new FolderBrowserDialog())
            {
                DialogResult result = dia.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(dia.SelectedPath))
                {
                    return dia.SelectedPath;
                }
            }

            return null;
        }

        private string ChooseFolderExtended()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        private void SetBackgroundWorker()
        {
            this.backgroundWorker = new BackgroundWorker();
            this.backgroundWorker.DoWork += this.StartAsyncConversion;
            this.backgroundWorker.ProgressChanged += this.ReportProgressToGUI;
            this.backgroundWorker.RunWorkerCompleted += this.AsyncConversionEnd;

        }

        private void ReportProgressToGUI(object sender, ProgressChangedEventArgs args)
        {
            // chilling
        }

        private void StartAsyncConversion(object sender, DoWorkEventArgs args)
        {
            if (!(args.Argument is ConvertorOptions)) { throw new ArgumentException(); }
            ConvertorOptions options = (ConvertorOptions)args.Argument;

            Convertor myConvertor = new Convertor();
            Exception possibleException = null;
            myConvertor.Convert(options, out possibleException);
        }

        private void AsyncConversionEnd(object sender, RunWorkerCompletedEventArgs args)
        {
            System.Windows.Forms.MessageBox.Show("Operace dokončena. Ještě bych měl říct s jakým výsledkem.");
        }

        private ConvertorOptions GetConvertorOptions()
        {
            ConvertorOptions options = new ConvertorOptions();
            options.FolderPath = this._selectedFolderPath;
            options.SourceEncoding = this._selectedEncodingSource;
            options.OutputEncoding = this._selectedEncodingOutput;
            options.consideredFileExtensions = this._consideredFileExtensions;

            return options;
        }
    }
}
